
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.cpfg;

import java.io.*;

import de.grogra.grammar.*;
import de.grogra.imp.io.XMLTableReader;
import de.grogra.util.*;
import de.grogra.pf.io.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import org.xml.sax.ext.LexicalHandler;

public class FunctionFilter extends SAXFilterBase
{
	private static final IOFlavor FLAVOR
		= new IOFlavor (XMLTableReader.MIME_TYPE, IOFlavor.SAX, null);

	private static final int RANGE = Token.MIN_UNUSED;
	private static final int POINTS = Token.MIN_UNUSED + 1;
	private static final int COLON = Token.MIN_UNUSED + 2;


	public FunctionFilter (FilterItem item, FilterSource source)
	{
		super (item, source, FLAVOR);
	}


	public void parse (ContentHandler ch, ErrorHandler eh, LexicalHandler lh,
					   DTDHandler dh, EntityResolver er)
		throws IOException, SAXException
	{
		Tokenizer t = new Tokenizer (Tokenizer.FLOAT_IS_DEFAULT
									 | Tokenizer.MINUS_IS_SIGN
									 | Tokenizer.EVALUATE_NUMBERS);
		t.addToken (RANGE, "range");
		t.addToken (POINTS, "points");
		t.addToken (COLON, ":");
		t.setSource (((ReaderSource) source).getReader (),
					 source.getSystemId ());
		try
		{
			t.consume (RANGE);
			t.consume (COLON);
			t.getFloat ();
			t.getFloat ();
			t.consume (POINTS);
			t.consume (COLON);
			int n = t.getInt ();

			ch.startDocument ();
			AttributesImpl atts = new SAXElement ();
			ch.startElement ("", "table", "table", atts);

			for (int i = 0; i < n; i++)
			{
				ch.startElement ("", "row", "row", atts);

				ch.startElement ("", "cell", "cell", atts);
				String s = String.valueOf (t.getFloat ());
				ch.characters (s.toCharArray (), 0, s.length ());
				ch.endElement ("", "cell", "cell");

				ch.startElement ("", "cell", "cell", atts);
				s = String.valueOf (t.getFloat ());
				ch.characters (s.toCharArray (), 0, s.length ());
				ch.endElement ("", "cell", "cell");

				ch.endElement ("", "row", "row");
			}

			ch.endElement ("", "table", "table");
			ch.endDocument ();
		}
		catch (RecognitionException e)
		{
			throw new IOWrapException (e);
		}
		t.getInput ().close ();
	}

}
