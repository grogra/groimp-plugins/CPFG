# CPFG

Support interface for the software L-Studio/cpfg. 

It includes support of the file types:

- ".s"
- ".func"
- ".con"
